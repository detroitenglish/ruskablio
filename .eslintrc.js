module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 9,
    parser: `babel-eslint`,
    sourceType: `module`,
    allowImportExportEverywhere: true,
    babelOptions: {
      configFile: `${__dirname}/babel.config.js`,
    },
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  globals: {
    JSP: `readonly`,
    RELEASE_ID: `readonly`,
    umami: `writable`,
    DB_HASH: `readonly`,
    PROD: `readonly`,
  },
  extends: [
    `eslint:recommended`,
    `plugin:vue/recommended`,
    `plugin:prettier/recommended`,
  ],
  plugins: [`babel`, `prettier`],
  rules: {
    'babel/no-invalid-this': 0,
    'babel/no-unused-expressions': 0,
    'babel/valid-typeof': 1,
    'vue/order-in-components': `warn`,
    'vue/multi-word-component-names': `off`,
    'vue/v-on-function-call': [`error`, `never`],
    'no-console': 0,
    'no-empty': `off`,
    'no-var': `error`,
    'prefer-template': `error`,
    quotes: [`warn`, `backtick`],
    eqeqeq: `error`,
    strict: `error`,
    'require-await': `error`,
    'prettier/prettier': [
      `warn`,
      {},
      {
        usePrettierrc: true,
      },
    ],
  },
}
