# Ruskablio

A hastily-built Progressive Web App for studying the 1,250 most commonly used words in Russian, as well as learning the Cyrillic alphabet. Once installed, the app does not need an internet connection to use.

## Requirements

Node 14.x

## Initial setup

### Dependency Installation

```
npm install
```

### Configuration Variables

Rename `example.env` to `.env` and set the appropriate variable accordingly.

## Development

### Compiles and hot-reloads for development

```
npm run serve
```

### Lints and fixes files

```
npm run lint
```

## Production Deployment

### Generates a custom icon font, compiles code for production and uploads files to production server

```
npm run deploy
```

## Webserver Config

There's a `Caddyfile` available if you're using Caddy v1.x web server. Otherwise, you can configure whatever web server you prefer by studying the `Caddyfile`. It's all static files, after all...
