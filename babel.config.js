const {
  devDependencies: { 'core-js': corejs },
} = require(`./package.json`)

const prod = process.env.NODE_ENV === `production`

module.exports = {
  presets: [
    [
      `@vue/cli-plugin-babel/preset`,
      {
        loose: true,
        corejs: +corejs.slice(1),
        modules: `auto`,
        useBuiltIns: `usage`,
      },
    ],
  ],
  plugins: [prod && `module:faster.js`, prod && `closure-elimination`].filter(
    Boolean
  ),
}
