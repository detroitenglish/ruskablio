/* eslint-disable no-undef */

let cacheName = `ruskablio-precache-v2-${self.registration.scope}`

function preCachePages() {
  caches.open(cacheName).then(cache => {
    cache.addAll(
      [``, `about`, `imprint`, `trainer`, `alphabet`].map(
        url => `${self.registration.scope}${url}`
      )
    )
  })
}

setTimeout(preCachePages, 3e3)

const IndexMatchCb = ({ url }) => {
  return /^\/(about|alphabet|imprint|trainer|index\.html)?$|\/worker\.js|manifest\.json/.test(
    url.pathname
  )
}

workbox.routing.registerRoute(
  IndexMatchCb,
  new workbox.strategies.NetworkFirst({ cacheName })
)

const assetMatchCb = ({ url }) => {
  return /\.(gif|webp|png|jpg)$/.test(url.pathname)
}

workbox.routing.registerRoute(
  assetMatchCb,
  new workbox.strategies.CacheFirst({ cacheName })
)

const metadataDb = ({ url }) => {
  return /\.(txt|map|xml)$|report\.html$/.test(url.pathname)
}

workbox.routing.registerRoute(
  metadataDb,
  new workbox.strategies.NetworkOnly({})
)
