#!/usr/bin/env zx

let { config } = require(`dotenv`)
config(`${__dirname}/../.env`)
const crypto = require(`crypto`)

const docs = require(`../src/assets/ruskablio.json`)

const docsHash = await crypto
  .createHash(`sha1`)
  .update(JSON.stringify(docs))
  .digest(`hex`)

console.log(chalk.blue(`DB HASH: ${docsHash}`))

docs.push({ _id: `hash`, hash: docsHash })

const content = `/*

Here, we are exploiting the fact that JS can parse a string into an object
faster than it can parse... an actual object. Hacky AF, but that's JS for you...

*/

export default JSON.parse\`${JSON.stringify(docs)}\`

`

await $`rm ${__dirname}/../src/assets/ruskablio.js`

await fs.writeFile(`${__dirname}/../src/assets/ruskablio.js`, content)

const dbHashContent = `{ "hash": "${docsHash}" }`

await $`rm ${__dirname}/../src/assets/db-hash.json`

await fs.writeFile(`${__dirname}/../src/assets/db-hash.json`, dbHashContent)
