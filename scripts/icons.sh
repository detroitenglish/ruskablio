#!/usr/bin/env bash

PATH=$PATH:$PWD/node_modules/.bin

mkdir -p $PWD/src/styles/icons/svg

CURRENT_SVGS=$( cat $PWD/src/styles/icons-list.txt || "" )

test -z $CI && grep -ohE "mdi-([a-z0-9\-]{2,})" --include="*.vue" --include="*.js" -r $PWD/src \
  | sort \
  | uniq \
  | sed -E "s/mdi-(.*)/\1.svg/g" \
  > $PWD/src/styles/icons-list.txt

SVGS=$( cat $PWD/src/styles/icons-list.txt )

if [[ ${CURRENT_SVGS[*]} == ${SVGS[*]} ]]; then
    echo "No icon changes found";
    exit 0;
fi

rm -f $PWD/src/styles/icons/svg/*.svg ;

for i in $SVGS; do
  if [ $i == "icons.svg" ]; then continue; fi;
  test -e $PWD/node_modules/@mdi/svg/svg/$i && \
    cp $PWD/node_modules/@mdi/svg/svg/$i $PWD/src/styles/icons/svg/ \
    || echo "$i NOT FOUND!" ;
done

icon-font-generator $PWD/src/styles/icons/svg/*.svg -s \
  -o $PWD/src/styles/icons \
  -n mdi-icons \
  -p mdi \
  --types="woff,woff2" \
  --html=false --json=false \
  -f "./icons" 2> /dev/null

test -z $CI && ex -s -c '3i|    font-display: swap;' -c x $PWD/src/styles/icons/mdi-icons.css
