#!/usr/bin/env zx

async function fixManifest() {
  let manifestPath = `${__dirname}/../dist/img/icons/manifest.json`

  let manifest = JSON.parse(await fs.readFile(manifestPath))

  for (let icon of manifest.icons) {
    icon.purpose = icon.src.includes(`144`) ? `any` : `maskable`
  }

  await fs.writeFile(manifestPath, JSON.stringify(manifest, null, 0))
}

async function fixPrecacheManifests() {
  let precacheManifests = await globby(`${__dirname}/../dist/precache-*`)

  for (let file of precacheManifests) {
    let txt = Buffer.from(await fs.readFile(file)).toString()
    txt = txt.replace(/"revis.*",\n/g, ``)
    // txt = txt.replace(/"\/js\/worker\.js"/, `"/js/worker.js?t=${Date.now()}"`)
    await $`rm ${file}`
    await fs.writeFile(file, txt)
  }
}

async function writeSiteMap() {
  let sitemapPath = `${__dirname}/../dist/sitemap.xml`

  let sitemap = Buffer.from(await fs.readFile(sitemapPath)).toString()

  sitemap = sitemap.replace(
    /\d{4}-\d{2}-\d{2}/g,
    new Date().toJSON().split(`T`)[0]
  )

  await $`rm ${sitemapPath}`

  await fs.writeFile(sitemapPath, sitemap)
}

async function minifyJS() {
  let rootJSFiles = await globby([
    `${__dirname}/../dist/*.js`,
    `${__dirname}/../dist/js/worker.js`,
  ])

  for (let file of rootJSFiles) {
    // let filename = file.split(`/`).pop()
    await $`npx terser --compress --mangle --ecma 2019 -o ${file} -- ${file}`
  }
}

async function deletePWAmanifest() {
  await $`rm ${__dirname}/../dist/manifest.json`
}

await Promise.all([
  fixManifest(),
  fixPrecacheManifests(),
  writeSiteMap(),
  minifyJS(),
  deletePWAmanifest(),
]).catch(err => console.error(err) || $`exit 1`)
