#!/usr/bin/env bash

source $PWD/.env

SRC="$PWD/dist/"
DEST="$PRODUCTION_SERVER_HOST:$PRODUCTION_SERVER_APP_ROOT/"

supersync () {
  SRC=$1;
  shift;
  DEST=$1;
  shift;
  rsync --exclude={node_modules,.git} "$@" -r -v -e "ssh -T -o Compression=no -x" "$SRC" "$DEST" ;
}

supersync "$SRC" "$DEST" --delete-excluded
