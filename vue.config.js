require(`dotenv`).config()
const prod = process.env.NODE_ENV === `production`
const webpack = require(`webpack`)
const AppManifestWebpackPlugin = require(`app-manifest-webpack-plugin`)
const HtmlPluginRemove = require(`html-webpack-plugin-remove`)
const { hash } = require(`./src/assets/db-hash.json`)
let { DEV_HOST = `localhost`, DEV_PORT = 8080 } = process.env

module.exports = {
  devServer: {
    disableHostCheck: true,
    host: DEV_HOST,
    port: DEV_PORT,
    headers: {
      'referrer-policy': `no-referrer`,
    },
  },
  lintOnSave: false,
  productionSourceMap: !process.env.FAST,
  css: {
    loaderOptions: {
      sass: {
        data: `@import '@/styles/variables.scss'`,
      },
    },
  },
  chainWebpack: config => {
    config.plugin(`html`).tap(args => {
      args[0].template = `./src/index.pug`
      return args
    })
    if (!prod) {
      config.plugins.delete(`pwa`)
      config.plugins.delete(`workbox`)
    }
  },
  configureWebpack: {
    node: prod ? false : void 0,
    resolve: {
      alias: {
        '@': `${__dirname}/src`,
      },
    },
    plugins: [
      new webpack.DefinePlugin({
        global: {},
        DB_HASH: JSON.stringify(hash),
        PROD: JSON.stringify(prod),
      }),
      prod &&
        new AppManifestWebpackPlugin({
          logo: `${__dirname}/public/img/logo.png`,
          prefix: `/img/icons/`,
          output: `img/icons/`,
          emitStats: false,
          persistentCache: false,
          inject: true,
          config: {
            background: `#ED1C24`,
            theme_color: `#3F48CC`,
            appName: `Ruskablio`,
            appDescription: `A simple app to study the 1,250 most common Russian words and learn the Cyrillic alphabet`,
            start_url: `/index.html`,
            display: `standalone`,
            lang: `en`,
            dir: `ltr`,
            icons: {
              android: true,
              appleIcon: true,
              appleStartup: false,
              coast: false,
              favicons: true,
              firefox: false,
              yandex: false,
              windows: true,
            },
          },
        }),

      new HtmlPluginRemove(/<link rel="manifest" href="\/manifest.json">/),
    ].filter(Boolean),
  },
  pwa: prod
    ? {
        name: `Ruskablio`,
        themeColor: `#3F48CC`,
        msTileColor: `#ED1C24`,
        workboxPluginMode: `GenerateSW`,
        workboxOptions: {
          exclude: [
            /\.(map|m3u|xml|webapp)$/,
            /iconstats|report|touch-icon|\.cache|mstile|android-chrome-[0-9]/,
            /logo-?|screenshot|firefox|apple|favicon|precache|vocab/,
            /\.(eot|ttf|woff|pug|jpg)$/,
            `manifest.json`,
            `worker.js`,
            `sitemap.xml`,
            `robots.txt`,
          ],
          navigateFallback: `/index.html`,
          cleanupOutdatedCaches: true,
          skipWaiting: true,
          clientsClaim: true,
          importScripts: [`/js/worker.js`],
        },
      }
    : void 0,
}
